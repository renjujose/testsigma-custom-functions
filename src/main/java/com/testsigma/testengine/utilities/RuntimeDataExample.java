package com.testsigma.testengine.utilities;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.testengine.exceptions.TestEngineException;

public class RuntimeDataExample extends TestsigmaCustomFunctions {

	@CustomTestStep
	public TestStepResult getRuntimeParameter(String name) throws TestEngineException {
		TestStepResult result = new TestStepResult();
		String testData = getRuntimeData(name);
		result.setStatus(ResultConstants.SUCCESS);
		result.setMessage("Custom step Executed successfully. Value of the runtime parameter "+name+" is "+testData);
		return result;
	}
}
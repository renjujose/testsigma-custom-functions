package com.testsigma.testengine.utilities;


import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class ParameterTestDataAccessExample extends TestsigmaCustomFunctions{

    @CustomTestStep
  public TestStepResult storeParameterTestData(String name, String value) throws TestEngineException{
    
    setTestDataParameterValue(name,value);
    TestStepResult result= new TestStepResult();
    result.setStatus(ResultConstants.SUCCESS);
    result.setMessage("Parameter Testdata variable "+name+" with value "+value+" has been stored successfully");
    return result;
  }
  
    @CustomTestStep
  public TestStepResult getParameterTestData(String name) throws TestEngineException{

    String testdata = getTestDataParamterValue(name);
    TestStepResult result= new TestStepResult();
    result.setStatus(ResultConstants.SUCCESS);
    result.setMessage("The fetched Parameter Testdata variable "+name+" has value: "+testdata);
    return result;
  }
}

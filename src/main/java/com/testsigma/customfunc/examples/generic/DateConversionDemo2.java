package com.testsigma.customfunc.examples.generic;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class DateConversionDemo2 extends TestsigmaCustomFunctions {

	@CustomTestStep
	public TestStepResult convertDateFormat(String runtimevarname, String inputFormat, String outputFormat, String modifiedRunTimeVarName){
		TestStepResult result = new TestStepResult();
		String value,convertedDate=null;
		try {
			value = getRuntimeData(runtimevarname);
			SimpleDateFormat parser = new SimpleDateFormat(inputFormat);
			Date thedate = parser.parse(value);
			SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
			convertedDate = formatter.format(thedate);
			System.out.println(convertedDate);
			setRuntimeData(modifiedRunTimeVarName, convertedDate);
		} catch (TestEngineException e) {
			e.printStackTrace();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Runtime Testdata variable "+ modifiedRunTimeVarName +" with value "+ convertedDate +" has been stored successfully");
		} catch (ParseException e) {
			e.printStackTrace();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Runtime Testdata variable "+ modifiedRunTimeVarName +" with value "+ convertedDate +" has been stored successfully");
		}
		return result;
	}

	@CustomTestStep
	public TestStepResult convertToNumber(String prefix, String suffix, String testDataProfileParamName, String modifiedRunTimeVarName){
		TestStepResult result = new TestStepResult();
		String convertedNum = null;
		try {
			String value = getTestDataParamterValue(testDataProfileParamName);
			convertedNum = "00";
			convertedNum = prefix+(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(value)))+suffix;
			setRuntimeData(modifiedRunTimeVarName, convertedNum);
		} catch (NumberFormatException e) {
			e.printStackTrace();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Runtime Testdata variable "+ modifiedRunTimeVarName +" with value "+ convertedNum +" has been stored successfully");
			return result;
		} catch (TestEngineException e) {
			e.printStackTrace();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Runtime Testdata variable "+ modifiedRunTimeVarName +" with value "+ convertedNum +" has been stored successfully");
		}
		return result;		
	}

}
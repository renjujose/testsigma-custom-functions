package com.testsigma.customfunc.examples.generic;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.remote.CapabilityType;


public class DeveloperToolsFullPageScreenshot {
WebDriver driver;


public void setUp() throws MalformedURLException{
	//Set up desired capabilities and pass the Android app-activity and app-package to Appium
	/*DesiredCapabilities capabilities = new DesiredCapabilities();
	capabilities.setCapability("BROWSER_NAME", "Android");
   capabilities.setCapability("appPackage", "com.android.calculator2");
// This package name of your app (you can get it from apk info app)
	capabilities.setCapability("appActivity","com.android.calculator2.Calculator"); // This is Launcher activity of your app (you can get it from apk info app)
//Create RemoteWebDriver instance and connect to the Appium server
 //It will launch the Calculator App in Android Device using the configurations specified in Desired Capabilities
*/  
	System.setProperty("webdriver.chrome.driver", "/home/renjous/Documents/Softwares/libs/binaries/chromedriver"); 
	driver = new ChromeDriver();
}


public void openDevTools() throws Exception {   
	driver.navigate().to(new URL("https://www.google.com"));
//	driver.findElement(By.xpath("//body")).sendKeys(Keys.chord((Keys.CONTROL),Keys.SHIFT,"i"));
//	driver.findElement(By.xpath("//body")).sendKeys(Keys.chord(Keys.chord((Keys.CONTROL),Keys.SHIFT),"i"));

	Actions actions = new Actions(driver);
	Action opendevtools = actions.sendKeys(Keys.CONTROL).sendKeys(Keys.SHIFT).sendKeys("i").build();
	opendevtools.perform();
	
//	Action switchtoconsole = actions.keyDown(Keys.CONTROL).sendKeys("]").keyUp(Keys.CONTROL).build();
//	switchtoconsole.perform();
	
	Thread.sleep(5000);
}


public void teardown(){
	//close the app
	driver.quit();
}

public static void main(String[] args) throws Exception {
	DeveloperToolsFullPageScreenshot devtoolswindow = new DeveloperToolsFullPageScreenshot();
	devtoolswindow.setUp();
	devtoolswindow.openDevTools();
	devtoolswindow.teardown();
}
}

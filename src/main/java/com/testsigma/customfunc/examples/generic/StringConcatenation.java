package com.testsigma.customfunc.examples.generic;

import org.openqa.selenium.WebDriver;

public class StringConcatenation {
	protected WebDriver driver;

	private String concatvariables(String var1, String var2, String resultvariable) {
		//TODO get values from runtime variables with names "var1", "var2"
		
		// concatenating the fetched values of runtime values
		String concatvar = var1.concat(var2);
		

		
		// Used in grammar - Store the value for the attribute @{attribute} of the element #{logicalname} into a variable ${testdata}
		//new RuntimeData().storeRuntimeVarible(executionID, variable, attributeValue, envSettings);
		
		return resultvariable;
		
	}
	
}

package com.testsigma.customfunc.examples.generic;

import java.util.HashMap;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class SwitchTabs {

	WebDriver driver = null;
	private Set<String> windows = null;
	private String currentwindow = null;
	HashMap<Integer, String> windowsmap = null;
	String linklocator = null;
	private int count = 0;
	
	@Test(priority=0)
	void init(){
		System.setProperty("webdriver.chrome.driver","/home/renjous/Documents/Softwares/libs/binaries/chromedriver");
		driver = new ChromeDriver();
	}
	
	@Test(priority=1)
	void performSteps() {
		driver.get("https://staging.nexthotels.com/");
		driver.findElement(By.xpath("//header[@id='desktop-header']//span[text()='Book Now']")).click();
		Select branddropdown = new Select(driver.findElement(By.xpath("//form[@id='book-engine']//select[@id='brand']")));
		branddropdown.selectByIndex(5);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Select hoteldropdown = new Select(driver.findElement(By.id("book_hotel")));
		hoteldropdown.selectByIndex(1);
		linklocator="//div[@class='cta-container']/input[@type='submit']";
		clickAndOpenTab();
	}
	
	void clickAndOpenTab() {
		//windows initialization
		windows = driver.getWindowHandles();
		currentwindow = driver.getWindowHandle();
		windowsmap = new HashMap<Integer, String>(windows.size());
		for(String window:windows) {
			windowsmap.put(count, window);
			count++;
		}
		WebElement linktoopen = driver.findElement(By.xpath(linklocator));
		linktoopen.click();
		
		// not needed in selenium tests but needed in testsigma framework
		//switchToTabByIndex(2);
	}
	
	private void switchToTabByIndex(int index) {
		driver.switchTo().window(windowsmap.get(index));
	}
}
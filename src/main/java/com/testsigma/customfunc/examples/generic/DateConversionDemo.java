package com.testsigma.customfunc.examples.generic;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.testsigma.customfunc.common.CustomTestData;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class DateConversionDemo extends TestsigmaCustomFunctions {

	@CustomTestData
	public String convertDateFormat(String runtimevarname, String inputFormat, String outputFormat){
		TestStepResult result = new TestStepResult();
		String value,convertedDate = null;
		try {
			value = getRuntimeData(runtimevarname);
			SimpleDateFormat parser = new SimpleDateFormat(inputFormat);
			Date thedate = parser.parse(value);
			SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
			convertedDate = formatter.format(thedate);
		} catch (TestEngineException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Runtime Testdata variable "+ runtimevarname +" with value "+ convertedDate +" has been stored successfully");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Runtime Testdata variable "+ runtimevarname +" with value "+ convertedDate +" has been stored successfully");
		}
		return convertedDate;
	}

	@CustomTestData
	public String convertToNumber(String prefix, String suffix, String testDataProfileParamName){
		TestStepResult result = new TestStepResult();
		String value,convertedNum = "00";
		try {
			value = getTestDataParamterValue(testDataProfileParamName);
			convertedNum = prefix+(NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(value)))+suffix;
		} catch (TestEngineException e) {
			e.printStackTrace();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Runtime Testdata variable "+ testDataProfileParamName +" with value "+ convertedNum +" has been stored successfully");
		}
		return convertedNum;
	}

}
package com.testsigma.customfunc.examples.generic;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

class ExcelFileContentChecker {

	public static void main(String[] args) {
		String filepath = "/home/renjous/Downloads/fieldDefinition.xlsx";
		String sheetname = "FieldDefinition";
		int rownum = 0, cellnum = 0;
		ExcelFileContentChecker xlcontentchecker = new ExcelFileContentChecker();
		System.out.println(xlcontentchecker.getCellContents(filepath, sheetname, rownum, cellnum));
		System.out.println(xlcontentchecker.getCellContents(filepath, sheetname, rownum).toString());
		System.out.println(xlcontentchecker.getCellContents(filepath, sheetname).toString());
		
	}

	String getCellContents(String filepath, String sheetname, int rownum, int cellnum) {
		Workbook xlworkbook = getWorkbook(filepath);
		Sheet xlsheet = xlworkbook.getSheet(sheetname);
		Row xlsheetrow = xlsheet.getRow(rownum);
		Cell xlsheetcell = xlsheetrow.getCell(cellnum);
		String data = xlsheetcell.getStringCellValue();
		return data;
	}

	ArrayList<String> getCellContents(String filepath, String sheetname, int rownum) {
		ArrayList<String> list = new ArrayList<String>();
		Workbook xlworkbook = getWorkbook(filepath);
		Sheet xlsheet = xlworkbook.getSheet(sheetname);
		Row xlrow = xlsheet.getRow(rownum);
		Iterator<Cell> xlsheetcellitr = xlrow.cellIterator();
		while (xlsheetcellitr.hasNext()) {
			list.add(xlsheetcellitr.next().getStringCellValue());
		}
		return list;
	}

	ArrayList<String> getCellContents(String filepath, String sheetname) {
		ArrayList<String> list = new ArrayList<String>();
		Workbook xlworkbook = getWorkbook(filepath);
		Sheet xlsheet = xlworkbook.getSheet(sheetname);
		Iterator<Row> xlsheetrowitr = xlsheet.rowIterator();
		while (xlsheetrowitr.hasNext()) {
			Iterator<Cell> xlsheetcellitr = xlsheetrowitr.next().cellIterator();
			while (xlsheetcellitr.hasNext()) {
				list.add(xlsheetcellitr.next().getStringCellValue());
			}
		}
		return list;
	}

	Workbook getWorkbook(String filepath) {
		FileInputStream fileinstrm;
		Workbook xlworkbook = null;
		File xlfile = new File(filepath);
		try {
			fileinstrm = new FileInputStream(xlfile);
			xlworkbook = WorkbookFactory.create(fileinstrm);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (EncryptedDocumentException | IOException e) {
			e.printStackTrace();
		}
		return xlworkbook;
	}

//        Iterator<Row> xlsheetrowitr = xlsheet.rowIterator();
//        while(xlsheetrowitr.hasNext()) {
//        	Iterator<Cell> xlsheetcellitr = xlsheetrowitr.next().cellIterator();
//        	while(xlsheetcellitr.hasNext()) {
//        		System.out.println(xlsheetcellitr.next().getStringCellValue());
//        	}
//        }
}
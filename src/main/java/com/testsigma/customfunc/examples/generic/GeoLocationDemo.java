package com.testsigma.customfunc.examples.generic;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ProcessBuilder.Redirect;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

//import java.io.File;
//import com.jacob.com.LibraryLoader;

public class GeoLocationDemo {
	WebDriver driver = null;
	
	public static void main(String[] args){
		GeoLocationDemo st = new GeoLocationDemo();
		//st.initChrome();
		try {
			st.samplepgm();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//st.kill();
 catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
		private void initChrome() {
		System.setProperty("webdriver.chrome.driver", "resources//drivers//chromedriver.exe");
		//DesiredCapabilities ieCaps = DesiredCapabilities.internetExplorer();
		//ieCaps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://the-internet.herokuapp.com/basic_auth");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("Initalized Chrome WebDriver session");
	}
	
	
	@SuppressWarnings("deprecation")
	private void initIE() {
		System.setProperty("webdriver.ie.driver", "resources//drivers//IEDriverServer.exe");
		//DesiredCapabilities ieCaps = DesiredCapabilities.internetExplorer();
		//ieCaps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://74.95.157.169/twinengines/default.aspx");
		//driver = new InternetExplorerDriver(ieCaps);
		driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		System.out.println("Initalized IE WebDriver session");
	}
	
	private void samplepgm() throws InterruptedException, IOException {
		
	}
	
	private void kill() {
		System.out.println("Quitting session in 3 seconds");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();
		System.out.println("Terminated session");
	}
}

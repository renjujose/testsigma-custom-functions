package com.testsigma.customfunc.examples.generic;

import java.lang.Float;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class SumCheck extends TestsigmaCustomFunctions{
	TestStepResult result= new TestStepResult();
  
    @CustomTestStep
  public TestStepResult checkSumofNumbers(String var1, String var2, String var3) throws TestEngineException{
    float firstnumber = Float.parseFloat(getRuntimeData(var1));
    float secondnumber = Float.parseFloat(getRuntimeData(var2));
    float sumofnumbers = Float.parseFloat(getRuntimeData(var3));
    if(firstnumber+secondnumber==sumofnumbers){
      result.setStatus(ResultConstants.SUCCESS);
      result.setMessage("Calculated Sum - "+(int)((int)firstnumber+(int)secondnumber)+" is equal to stored sum of numbers - "+sumofnumbers);
    }
    else{
      result.setStatus(ResultConstants.FAILURE);
      result.setMessage("Calculated Sum not equal to actual sum of numbers");  
    }
    return result;
  }
}
package com.testsigma.customfunc.examples.generic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.testng.annotations.Test;

public class StringMatchRegexDemo {
	
	@Test
	public String matchpattern(String tomatch, String inputstring) {
	String tempresult=null,result;
	tomatch = "\"message\":.*\"}";
	inputstring = "Testsigma webdriver exception, details :unknown error: unhandled inspector error: {\"code\":-32000,\"message\":\"Cannot navigate to invalid URL\"} (Session info: chrome=69.0.3481.0) (Driver info: chromedriver=2.39.562718 (9a2698cba08cf5a471a29d30c8b3e12becabb0e9),platform=Windows NT 6.1.7601 SP1 x86_64) (WARNING: The server did not provide any stacktrace information) Command duration or timeout: 0 milliseconds Build info: version: '3.6.0', revision: '6fbf3ec767', time: '2017-09-27T15:28:36.4Z' System info: host: 'ip-172-31-26-86.ap-south-1.compute.internal', ip: '172.31.26.86', os.name: 'Linux', os.arch: 'amd64', os.version: '3.10.0-693.11.6.el7.x86_64', java.version: '1.8.0_161' Driver info: org.openqa.selenium.remote.RemoteWebDriver Capabilities [{mobileEmulationEnabled=false, hasTouchScreen=false, platform=XP, acceptSslCerts=false, acceptInsecureCerts=false, webStorageEnabled=true, takesScreenshot=true, browserName=chrome, javascriptEnabled=true, platformName=XP, setWindowRect=true, unexpectedAlertBehaviour=, applicationCacheEnabled=false, rotatable=false, networkConnectionEnabled=false, chrome={chromedriverVersion=2.39.562718 (9a2698cba08cf5a471a29d30c8b3e12becabb0e9), userDataDir=C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\scoped_dir1588_4767}, takesHeapSnapshot=true, unhandledPromptBehavior=, pageLoadStrategy=normal, hasMetadata=true, handlesAlerts=true, databaseEnabled=false, version=69.0.3481.0, browserConnectionEnabled=false, nativeEvents=true, webdriver.remote.sessionid=8dfa44e5b7864a9996c0b67d2dbb7573, locationContextEnabled=true, cssSelectorsEnabled=true}] Session ID: 8dfa44e5b7864a9996c0b67d2dbb7573";
	Pattern pattern = Pattern.compile(tomatch);
	Matcher matcher = 	pattern.matcher(inputstring);
	while(matcher.find()){
		System.out.print("Start index: " + matcher.start());
        System.out.print(" End index: " + matcher.end() + " ");
        tempresult = matcher.group();
	}
	System.out.println("Beforereplace: "+tempresult);
	tempresult = tempresult.replaceAll("\"message\":\"", "");
	result = tempresult.replaceAll("\"}", "");
	System.out.println("Afterreplace: "+result);
	return result;
	}
	
	@Test
	public String getDriverErrorMessage(String inputstring) {
		String tomatch = "\"message\":.*\"}";
		inputstring = "Testsigma webdriver exception, details :unknown error: unhandled inspector error: {\"code\":-32000,\"message\":\"Cannot navigate to invalid URL\"} (Session info: chrome=69.0.3481.0) (Driver info: chromedriver=2.39.562718 (9a2698cba08cf5a471a29d30c8b3e12becabb0e9),platform=Windows NT 6.1.7601 SP1 x86_64) (WARNING: The server did not provide any stacktrace information) Command duration or timeout: 0 milliseconds Build info: version: '3.6.0', revision: '6fbf3ec767', time: '2017-09-27T15:28:36.4Z' System info: host: 'ip-172-31-26-86.ap-south-1.compute.internal', ip: '172.31.26.86', os.name: 'Linux', os.arch: 'amd64', os.version: '3.10.0-693.11.6.el7.x86_64', java.version: '1.8.0_161' Driver info: org.openqa.selenium.remote.RemoteWebDriver Capabilities [{mobileEmulationEnabled=false, hasTouchScreen=false, platform=XP, acceptSslCerts=false, acceptInsecureCerts=false, webStorageEnabled=true, takesScreenshot=true, browserName=chrome, javascriptEnabled=true, platformName=XP, setWindowRect=true, unexpectedAlertBehaviour=, applicationCacheEnabled=false, rotatable=false, networkConnectionEnabled=false, chrome={chromedriverVersion=2.39.562718 (9a2698cba08cf5a471a29d30c8b3e12becabb0e9), userDataDir=C:\\Users\\ADMINI~1\\AppData\\Local\\Temp\\scoped_dir1588_4767}, takesHeapSnapshot=true, unhandledPromptBehavior=, pageLoadStrategy=normal, hasMetadata=true, handlesAlerts=true, databaseEnabled=false, version=69.0.3481.0, browserConnectionEnabled=false, nativeEvents=true, webdriver.remote.sessionid=8dfa44e5b7864a9996c0b67d2dbb7573, locationContextEnabled=true, cssSelectorsEnabled=true}] Session ID: 8dfa44e5b7864a9996c0b67d2dbb7573";
		String tempresult=null;
		String result=null;
		Pattern pattern= Pattern.compile(tomatch);
		Matcher matcher = 	pattern.matcher(inputstring);
		while(matcher.find()){
			System.out.print("Start index: " + matcher.start());
	        System.out.print(" End index: " + matcher.end() + " ");
	        tempresult = matcher.group();
		}
		try {
			System.out.println("Beforereplace: "+tempresult);
			tempresult = tempresult.replaceAll("\"message\":\"", "");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getMessage().substring(68, 98);
		}
		result = tempresult.replaceAll("\"}", "");
		System.out.println("Afterreplace: "+result);
		return result;
	}
}

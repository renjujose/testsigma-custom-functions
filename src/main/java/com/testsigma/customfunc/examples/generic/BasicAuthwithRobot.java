package com.testsigma.customfunc.examples.generic;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

//import java.io.File;
//import com.jacob.com.LibraryLoader;

public class BasicAuthwithRobot {
	WebDriver driver = null;
	Robot robot=null;
	
	public static void main(String[] args){
		BasicAuthwithRobot st = new BasicAuthwithRobot();
		st.initIE();
		try {
			st.samplepgm();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//st.kill();
	}
	
		private void initChrome() {
		System.setProperty("webdriver.chrome.driver", "resources//drivers//chromedriver.exe");
		//DesiredCapabilities ieCaps = DesiredCapabilities.internetExplorer();
		//ieCaps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://the-internet.herokuapp.com/basic_auth");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		System.out.println("Initalized Chrome WebDriver session");
	}
	
	
	@SuppressWarnings("deprecation")
	private void initIE() {
		System.setProperty("webdriver.ie.driver", "resources//drivers//IEDriverServer.exe");
		//DesiredCapabilities ieCaps = DesiredCapabilities.internetExplorer();
		//ieCaps.setCapability(InternetExplorerDriver.INITIAL_BROWSER_URL, "http://74.95.157.169/twinengines/default.aspx");
		//driver = new InternetExplorerDriver(ieCaps);
		driver = new InternetExplorerDriver();
		driver.manage().window().maximize();
		System.out.println("Initalized IE WebDriver session");
	}
	
	private void samplepgm() throws InterruptedException {
		
		//driver.get("http://74.95.157.169/twinengines/default.aspx");
		driver.get("http://the-internet.herokuapp.com/basic_auth");
		try {
			Thread.sleep(3000);
			//entercredswithRobot("TestUser_Admin", "2Engines");
			entercredswithRobot("admin", "admin");
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (AWTException e) {
			e.printStackTrace();
		}
		System.out.println("Check whether driver is alive or not");
		Thread.sleep(3000);
		//driver.switchTo().activeElement();
		driver.findElement(By.xpath("//*[text()='Elemental Selenium']")).click();
		//driver.findElement(By.xpath("//*[text()='Case Management']")).click();
	}
	
	public void entercredswithRobot(String username, String password) throws InterruptedException, AWTException {
		robot = new Robot();
		StringSelection copyusernamecontent = new StringSelection(username);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(copyusernamecontent, null);
        Thread.sleep(4000);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_TAB);
        Thread.sleep(3000);
        StringSelection copypasswordcontent = new StringSelection(password);
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(copypasswordcontent, null);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        Thread.sleep(1000);
		System.out.println("Entered data using robot method");
	}	


	private void kill() {
		System.out.println("Quitting session in 3 seconds");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.quit();
		System.out.println("Terminated session");
	}
	
	/*	public static String getJvmBitVersion(){
		return System.getProperty("sun.arch.data.model");
		}
	
		public void entercredswithAutoit(String username, String password) throws InterruptedException {
		AutoItX x = new AutoItX();
		x.winActivate("Windows Security");
		x.winWaitActive("Windows Security");
		//Enter username and press tab
		x.send(username+"{TAB}");
		//Enter password and press enter
		x.send(password+"{ENTER}");
		System.out.println("Entered robot method");
	}
	
		private void setDeps() {
		String jacobDllVersionToUse;
		if (getJvmBitVersion().contains("32")){
			jacobDllVersionToUse = "jacob-1.19-x86.dll";
		}
		else {
			jacobDllVersionToUse = "jacob-1.19-x64.dll";
		}
		File file = new File("lib", jacobDllVersionToUse);
		System.load(file.getAbsolutePath());
		System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
	}*/
}

package com.testsigma.customfunc.examples.generic;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;

public class DateConversionDemo3 extends TestsigmaCustomFunctions {
	TestStepResult result = new TestStepResult();

	@CustomTestStep
	public TestStepResult convertDateFormat(String runtimevarname, String inputFormat, String outputFormat,
			String modifiedRunTimeVarName) throws Exception {
		String convertedDate = "";
		String value = "";
		try {
			value = getRuntimeData(runtimevarname);
			SimpleDateFormat parser = new SimpleDateFormat(inputFormat);
			Date thedate = parser.parse(value);
			SimpleDateFormat formatter = new SimpleDateFormat(outputFormat);
			convertedDate = formatter.format(thedate);
			System.out.println(convertedDate);
			setRuntimeData(modifiedRunTimeVarName, convertedDate);
		} catch (TestEngineException testngex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Runtime Testdata variable not written successfully" + testngex.getMessage());
		} catch (ParseException pex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Runtime Testdata variable unable to parse" + pex.getMessage());
		}
		return result;
	}

	@CustomTestStep
	public TestStepResult convertToNumber(String prefix, String suffix, String testDataProfileParamName,
			String modifiedRunTimeVarName) throws Exception {
		String convertedNum = "";
		try {
			String value = getTestDataParamterValue(testDataProfileParamName);
			convertedNum = prefix + (NumberFormat.getNumberInstance(Locale.US).format(Integer.parseInt(value)))
					+ suffix;
			setRuntimeData(modifiedRunTimeVarName, convertedNum);
		} catch (TestEngineException testngex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Runtime Testdata variable not written successfully" + testngex.getMessage());
		}
		return result;
	}
}
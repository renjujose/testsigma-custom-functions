package com.testsigma.customfunc.examples.generic;
public class StringReplace {
	String inittext = "abc123@!#adsda";
	public boolean updateRuntimeData(String runtimeVariableName, String searchtext, String replacetext){
		boolean updatesuccess=false;
		if(inittext.contains(searchtext)){
			System.out.println("Before replace :"+inittext);
			String finaltext = inittext.replace(searchtext, replacetext);
			System.out.println("After replace :"+finaltext);
			updatesuccess = true;
		}
		else{
			System.out.println("search Text not found: "+inittext);
		}
		return updatesuccess;
	}




	public static void main(String[] args) {
		StringReplace sr = new StringReplace();
		if(sr.updateRuntimeData(sr.inittext, "12", "34")){
			System.out.println("success");
		}
		else{
			System.out.println("failure");
		}
	}
}
package com.testsigma.customfunc.examples.rest;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.openqa.selenium.WebDriver;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;


public class TestsigmaAPICustFnDemo{
	private static WebDriver driver;
	private static Long envID;
	private static boolean isDry;
	private static String variablename;
	private final String USERNAME="admin@testsigma.com";
	private final String PASSWORD="admin";
	public TestsigmaAPICustFnDemo(){
		try {
			this.startExecWithBasicAuth();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TestsigmaAPICustFnDemo(WebDriver driver, Long envID, boolean isDry){	
		//TODO add env constr code
		try {
			this.getUIIdentifierWithBasicAuth();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public TestsigmaAPICustFnDemo(WebDriver driver, Long envID, boolean isDry, String variablename){	
		//TODO add env constr code
		try {
			this.storeRuntimeParamWithBasicAuth();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public TestStepResult startExecWithBasicAuth() throws ClientProtocolException, IOException {
		TestStepResult result = new TestStepResult();
		HttpPost request = new HttpPost("https://app.testsigma.com/rest/projects");
		String auth = USERNAME + ":" + PASSWORD;
		byte[] encodedAuth = Base64.encodeBase64(
		  auth.getBytes(StandardCharsets.ISO_8859_1));
		String authHeader = "Basic " + new String(encodedAuth);
		request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		if(statusCode==HttpStatus.SC_OK) {
			System.out.println("Success");
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Custom step executed successfully");
		}
		else {
			System.out.println("BAD REQUEST CONTENT"+statusCode);
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Bad Request Content"+statusCode);
		}
		return result;
	}
	
	public static String getBasicAuthString(String s) {
		try {			
			byte[] encodedAuth = Base64.encodeBase64(s.getBytes(Charset.forName("ISO-8859-1")));
			String authHeader = new String(encodedAuth);		
			return authHeader;
		} catch (Exception ignore) {
			return "";
		}
	}

	
	public static void testAPI() throws ClientProtocolException, IOException {
		HttpGet request = new HttpGet("http://localhost:8080/rest/projects/21");
		//HttpPost request = new HttpPost("https://app.testsigma.com/rest/execution/1229/run");
		String encodedAuth = getBasicAuthString("admin@testsigma.com:admin");
		String authHeader = "Basic " + new String(encodedAuth);
		request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		//String auth = USERNAME + ":" + PASSWORD;
		//byte[] encodedAuth = Base64.encodeBase64(
		//  auth.getBytes(StandardCharsets.ISO_8859_1));
		//String authHeader = "Basic " + new String(encodedAuth);
		//request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		if(statusCode==HttpStatus.SC_OK) {
			System.out.println("Success"+statusCode+response.getEntity());
		}
		else {
			System.out.println("Failed"+statusCode);
		}
	}
	
	//public TestStepResult storeRuntimeParamWithBasicAuth(String variable, String uiidname, String envRunId) throws ClientProtocolException, IOException {
	public TestStepResult storeRuntimeParamWithBasicAuth() throws ClientProtocolException, IOException {
		TestStepResult result = new TestStepResult();
		HttpPost request = new HttpPost("http://app.testsigma.com/rest/result/runtime-data/<runtimevaribleName>?envRunId=<environmentRunId>&isDry=isDry");
		String auth = "admin@testsigma.com" + ":" + "admin";
		byte[] encodedAuth = Base64.encodeBase64(
		  auth.getBytes(StandardCharsets.ISO_8859_1));
		String authHeader = "Basic " + new String(encodedAuth);
		request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		if(statusCode==HttpStatus.SC_OK) {
			System.out.println("Success");
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Custom step executed successfully");
		}
		else {
			System.out.println("BAD REQUEST CONTENT"+statusCode);
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Bad Request Content"+statusCode);
		}
		return result;
	}
	
	public TestStepResult getUIIdentifierWithBasicAuth() throws ClientProtocolException, IOException {
		TestStepResult result = new TestStepResult();
		HttpPost request = new HttpPost("http://app.testsigma.com/rest/ui-identifier?uiidentiifierename=gsearchbox&envRunId=4077&isDry=isDry");
		String auth = "admin@testsigma.com" + ":" + "admin";
		byte[] encodedAuth = Base64.encodeBase64(
		  auth.getBytes(StandardCharsets.ISO_8859_1));
		String authHeader = "Basic " + new String(encodedAuth);
		request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
		HttpClient client = HttpClientBuilder.create().build();
		HttpResponse response = client.execute(request);
		int statusCode = response.getStatusLine().getStatusCode();
		if(statusCode==HttpStatus.SC_OK) {
			System.out.println("Success");
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Custom step executed successfully");
		}
		else {
			System.out.println("BAD REQUEST CONTENT"+statusCode);
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Bad Request Content"+statusCode);
		}
		return result;
	}
	
	
	public static void main(String[] args) throws ClientProtocolException, IOException {
		//new TestsigmaAPICustFnDemo();
		TestsigmaAPICustFnDemo.testAPI();
		//new TestsigmaAPICustFnDemo(driver,envID,isDry);
		//new TestsigmaAPICustFnDemo(driver,envID,isDry,variablename);
	}
}

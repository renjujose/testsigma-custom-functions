package com.testsigma.customfunc.examples.rest;


import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;


public class RestAPIVerifyStatusCode extends TestsigmaCustomFunctions{

	@CustomTestStep
	public TestStepResult callRESTURL(String testdataparametername){

		TestStepResult result= new TestStepResult();
		int statusCode = 0;
		
		String Rurl;
		try {
			Rurl = getTestDataParameterValue(testdataparametername);
			HttpUriRequest request = new HttpGet(Rurl);
			HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
			statusCode = httpResponse.getStatusLine().getStatusCode();
			if(statusCode == 200)
			{
				result.setStatus(ResultConstants.SUCCESS);
				result.setMessage("Page loaded successfully. Returned 200 OK Status!");
			}
			else
			{
				result.setStatus(ResultConstants.FAILURE);
				result.setMessage("Page didn't load successfully. Returned "+statusCode+" Status!");
			}
		} catch (TestEngineException e) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to fetch Test data from Test Data Profile!");
		} catch (ClientProtocolException e) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to make REST Call!");
		} catch (IOException e) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to make REST Call!");
		}
		
		return result;
	}
}

package com.testsigma.customfunc.examples.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class StringToIntConv {

	
	@Test
	public void converttoInt(){
		String str = "100";
		System.out.println(str);
		int intstr = Integer.parseInt(str);
		System.out.println(intstr);	
		
		WebDriver driver = new ChromeDriver();
		
		Actions builder = new Actions(driver);
		
	    WebElement fromelement = driver.findElement(By.xpath("//div[@id='draggable']"));
	    WebElement toelement = driver.findElement(By.xpath("//div[@id='droppable']"));
	    int tocoordsx = toelement.getLocation().getX()+(toelement.getSize().getWidth()/2);
	    int tocoordsy = toelement.getLocation().getY()+(toelement.getSize().getHeight()*2);
	    
	    builder.clickAndHold(fromelement).pause(2000).moveToElement(toelement, toelement.getSize().getWidth()/2, toelement.getSize().getHeight()*2).pause(2000).release().pause(2000).build().perform();
	    //builder.dragAndDropBy(fromelement, tocoordsx,tocoordsy).perform();
	    builder.dragAndDrop(fromelement, toelement).perform();
	    
		
		
	}
		
		
}
	


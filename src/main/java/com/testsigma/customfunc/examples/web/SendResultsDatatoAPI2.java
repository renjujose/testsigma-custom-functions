package com.testsigma.customfunc.examples.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.apache.http.HttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.apache.http.util.EntityUtils;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;


//import static org.junit.Assert.assertThat;
//import static org.hamcrest.Matchers.equalTo;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class SendResultsDatatoAPI2 extends TestsigmaCustomFunctions {

  
	@CustomTestStep
	public TestStepResult iterateforTPFT() {
      	int i=0; //for getting count of iterations after iterating through the list
      	String lastquote=""; //for adding an output of last quote details sent to the API to the test step success message
		TestStepResult result = new TestStepResult();
		
      	try {	
      		String PageUrl = getRuntimeData("PageUrl");
			String QuoteReference = getRuntimeData("QuoteReference");
			String brokerNameXpath = "(//img[contains(@id,'brokerImg') and contains(@id,'quotesListTPFT')])[";
			String brokerPriceXpath = "(//span[contains(@id,'ctl00_premium') and"
              +" contains(@id,'quotesListTPFT') and not(contains(@id,'Copy'))])[";
			String brokerPositionXpath = "(//span[contains(@id,'ctl00_position') and"
              +" contains(@id,'quotesListTPFT')])[";
			String tPFTCoverType = getRuntimeData("TPFTCover");
			int itrCount = Integer.parseInt(getRuntimeData("tPFTtablerowsnumber"));
			
			String allApiStatusCodes=" ";
          	for (i = 1; i <= itrCount; i++) {
				String tPFTBrokerName = driver.findElement(By.xpath(brokerNameXpath+i+"]"))
						.getAttribute("title");
				String tPFTBrokerPrice = driver.findElement(By.xpath(brokerPriceXpath+i+"]")).getText();
				String tPFTBrokerPosition = driver.findElement(By.xpath(brokerPositionXpath+i+"]")).getText();
				int apiResponseStatus = senddetailstoAPI(PageUrl, QuoteReference, tPFTCoverType, tPFTBrokerName, tPFTBrokerPrice,
						tPFTBrokerPosition);
				allApiStatusCodes = allApiStatusCodes+" ,"+i+". "+apiResponseStatus;
              	if(i==itrCount){
                  lastquote = "Last row of quote details sent to api - API Status Code: "+apiResponseStatus+", Cover Type: "+tPFTCoverType
                    +", Broker name: "+tPFTBrokerName+", Broker price: "+tPFTBrokerPrice
                    +", Broker position: "+tPFTBrokerPosition+"AllstatusCodes"+allApiStatusCodes;
                }
			}
	        result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Successfully sent quote details to API "+Integer.toString(i-1)+" times."+lastquote);
		} catch (NumberFormatException ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("NumberFormatException : Custom Step execution failed");
		} catch (NoSuchElementException nosuchelex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("NoSuchElementException : Custom Step execution failed");
		} catch (TestEngineException testengineex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("TestEngineException : Custom Step execution failed");
		} catch (ClientProtocolException clientprotex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("ClientProtocolException : Custom Step execution failed");
		} catch (IOException ioex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("IOException : Custom Step execution failed");
		}catch (Exception ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Exception : Custom Step execution failed"+ex.getMessage());
		}
      return result;
	}

  
  
  
	@CustomTestStep
	public TestStepResult iterateforTPO() {
      	int i=0;
      	String lastquote="";
		TestStepResult result = new TestStepResult();
		
      	try {
			String PageUrl = getRuntimeData("PageUrl");
			String QuoteReference = getRuntimeData("QuoteReference");
			String brokerNameXpath = "(//img[contains(@id,'brokerImg') and contains(@id,'quotesListTPO')])[";
			String brokerPriceXpath = "(//span[contains(@id,'ctl00_premium') and "
              +"contains(@id,'quotesListTPO') and not(contains(@id,'Copy'))])[";
			String brokerPositionXpath = "(//span[contains(@id,'ctl00_position') and "
              +"contains(@id,'quotesListTPO')])[";
			String tPOCoverType = getRuntimeData("TPOCover");
			int itrCount = Integer.parseInt(getRuntimeData("tPOtablerowsnumber"));

			for (i = 1; i <= itrCount; i++) {
				String tPOBrokerName = driver.findElement(By.xpath(brokerNameXpath + i + "]")).getAttribute("title");
				String tPOBrokerPrice = driver.findElement(By.xpath(brokerPriceXpath + i + "]")).getText();
				String tPOBrokerPosition = driver.findElement(By.xpath(brokerPositionXpath + i + "]")).getText();
				int apiResponseStatus = senddetailstoAPI(PageUrl, QuoteReference, tPOCoverType, tPOBrokerName, tPOBrokerPrice, tPOBrokerPosition);
              	if(i==itrCount){
                  lastquote = "Last row of Quote details sent to api - API Status Code: "+apiResponseStatus+", Cover Type: "
                    +tPOCoverType+", Broker name: "+tPOBrokerName+" ,Broker price: "
                    +tPOBrokerPrice+", Broker position: "+tPOBrokerPosition;
                }
			}
          	result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Successfully sent quote details to API "+Integer.toString(i-1)+" times."+lastquote);
		} catch (NumberFormatException ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("NumberFormatException : Custom Step execution failed"+i);
		} catch (NoSuchElementException nosuchelex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("NoSuchElementException : Custom Step execution failed"+i);
		} catch (TestEngineException testengineex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("TestEngineException : Custom Step execution failed"+i);
		} catch (ClientProtocolException clientprotex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("ClientProtocolException : Custom Step execution failed"+i);
		} catch (IOException ioex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("IOException : Custom Step execution failed"+i);
		}catch (Exception ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Exception : Custom Step execution failed"+i+ex.getMessage());
		}
      return result;
	}

	@CustomTestStep
	public TestStepResult iterateforComp() {
      	int i=0;
      	String lastquote="";
		TestStepResult result = new TestStepResult();
      
		try {
			String PageUrl = getRuntimeData("PageUrl");
			String QuoteReference = getRuntimeData("QuoteReference");
			String brokerNameXpath = "(//img[contains(@id,'brokerImg') and contains(@id,'quotesListC')])[";
			String brokerPriceXpath = "(//span[contains(@id,'ctl00_premium') and "
              +"contains(@id,'quotesListC') and not(contains(@id,'Copy'))])[";
			String brokerPositionXpath = "(//span[contains(@id,'ctl00_position') and "
              +"contains(@id,'quotesListC')])[";
			String compCoverType = getRuntimeData("CompCover");
			int itrCount = Integer.parseInt(getRuntimeData("comptablerowsnumber"));

			for (i = 1; i <= itrCount; i++) {
				String compBrokerName = driver.findElement(By.xpath(brokerNameXpath + i + "]"))
						.getAttribute("title");
				String compBrokerPrice = driver.findElement(By.xpath(brokerPriceXpath + i + "]")).getText();
				String compBrokerPosition = driver.findElement(By.xpath(brokerPositionXpath + i + "]")).getText();
				int apiResponseStatus = senddetailstoAPI(PageUrl, QuoteReference, compCoverType, compBrokerName, compBrokerPrice,
						compBrokerPosition);
              	if(i==itrCount){
                  lastquote = "Last row of Quote details sent to api - API Status Code: "+apiResponseStatus+", Cover Type: "
                    +compCoverType+", Broker name: "+compBrokerName+", Broker price: "
                    +compBrokerPrice+", Broker position: "+compBrokerPosition;
                }
			}
          	result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Successfully sent quote details to API "+
                              Integer.toString(i-1)+" times."+lastquote);
		} catch (NumberFormatException ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("NumberFormatException : Custom Step execution failed"+i);
		} catch (NoSuchElementException nosuchelex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("NoSuchElementException : Custom Step execution failed"+i);
		} catch (TestEngineException testengineex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("TestEngineException : Custom Step execution failed"+i);
		} catch (ClientProtocolException clientprotex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("ClientProtocolException : Custom Step execution failed"+i);
		} catch (IOException ioex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("IOException : Custom Step execution failed"+i);
		}catch (Exception ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Exception : Custom Step execution failed"+i+ex.getMessage());
		}
      return result;
	}

	public int senddetailstoAPI(String pageUrl, String quoteReference, String coverType, String brokerName,
			String brokerPrice, String brokerPosition) throws ClientProtocolException, IOException {
       	
		final HttpClient httpClient = HttpClients.createDefault();
		final HttpPost httpPost = new HttpPost("https://broker-testing.theinsurers.co.uk");

		httpPost.setHeader("x-api-key", "jInCzZB6eP73nlXRA75RC9yygOyTfHc96IAL6DNQ");
        httpPost.addHeader("Content-Type", "application/json");

		List<BasicNameValuePair> payload = new ArrayList<BasicNameValuePair>();
		payload.add(new BasicNameValuePair("QuoteReference", quoteReference));
		payload.add(new BasicNameValuePair("Cover type", coverType));
		payload.add(new BasicNameValuePair("Broker Name", brokerName));
		payload.add(new BasicNameValuePair("Broker Price", brokerPrice));
		payload.add(new BasicNameValuePair("Environment", pageUrl));
		payload.add(new BasicNameValuePair("Broker Position", brokerPosition));

		httpPost.setEntity(new UrlEncodedFormEntity(payload));  
        final HttpResponse httpResponse = httpClient.execute(httpPost);
        return httpResponse.getStatusLine().getStatusCode();
	}
}

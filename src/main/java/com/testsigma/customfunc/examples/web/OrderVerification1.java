package com.testsigma.customfunc.examples.web;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestsigmaTestdataNotFoundException;
import org.openqa.selenium.WebElement;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import org.openqa.selenium.By;

public class OrderVerification1 extends TestsigmaCustomFunctions{
  	TestStepResult result= new TestStepResult();
    boolean isArraySame = false;

    @CustomTestStep
    public TestStepResult verifyDescOrder(String commonxpath) throws TestsigmaTestdataNotFoundException{
      List<Float> pricelist =new ArrayList<Float>();
      try{
        //get list of price elements
		List<WebElement> price_elements = driver.findElements(By.xpath(commonxpath));
        //iterate,get text from elements and store it to pricelist
 		for(WebElement price_element : price_elements){
          String price = price_element.getText();
          pricelist.add(Float.parseFloat(price.replace("$", "").replace("C", "")));
		}
		List<Float> descpricelist =new ArrayList<Float>();
        //copy pricelist to descpricelist
		descpricelist.addAll(pricelist);
        //sort in descending order
        Collections.sort(descpricelist,Collections.reverseOrder());
		if(descpricelist.equals(pricelist)){
          isArraySame=true;
          result.setStatus(ResultConstants.SUCCESS);
          result.setMessage("Prices in given order: "+pricelist+System.lineSeparator()+System.lineSeparator()+"Prices are in Descending order");
		}else{
          result.setStatus(ResultConstants.FAILURE);
          result.setMessage("Prices in given order: "+pricelist+System.lineSeparator()+System.lineSeparator()+"Prices are not in Descending order");
        }
      }catch(Exception e){
        result.setStatus(ResultConstants.FAILURE);
        result.setMessage(e.getMessage());
        return result;
      }
      return result;
    }
}
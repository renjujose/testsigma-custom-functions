package com.testsigma.customfunc.examples.web;
/*You need to remove this package declaration when using
 *  this code in Custom Functions interface in Testsigma*/


/*You need to import at least the following four classes
 *  for the Custom Functions code to work. 
 *  More Classes can be imported if required.*/
import org.openqa.selenium.WebDriver;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;


public class CustomStepsSample {
/*You need to create a static instance of Selenium WebDriver class.
 * This can be instantiated in the Constructor below.
 * Constructor call with WebDriver instance is necessary. */
	static WebDriver webdriver;
	public CustomStepsSample(WebDriver webdriver){
		this.webdriver = webdriver;
	}

/*Use the @CustomTestStep custom annotation to mark
 *  the Custom Function you created. You may add other methods that
 *  doesn't have the annotation. Methods with this annotation will
 *  be converted to Custom Function after compilation. The Custom Functions
 *  should return an object of TestStepResult class. Use the setters in
 *  this class to set the message to be passed when the method
 *   is executed(Passed or failed.*/

	@CustomTestStep
	public TestStepResult createCustomSteps(){
/*<----------------Add your Java Code here----------------->*/		
/*You can delete this comment and add your Custom Java code here. */
		TestStepResult result = new TestStepResult();
		result.setStatus(ResultConstants.SUCCESS);
		result.setMessage("Successfully Executed");
		return result;
	}		
/* Using the setStatus and setMessage methods, we can set the message
 * to be shown when the code is executed. Return an object of the
 *  TestStepResult Class after setting the message
 * and status. */
}
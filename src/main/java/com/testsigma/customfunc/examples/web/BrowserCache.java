package com.testsigma.customfunc.examples.web;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BrowserCache {
	static WebDriver driver;

	public static void main(String[] args) {
		File file = new File("/Users/renju/Testsigma-Agent/drivers/googlechrome/78.0/chromedriver");
		if(file.exists()) {
			System.out.println("File exists");
			if(file.canRead() && file.canWrite() && file.canExecute()) {
				System.out.println("File readable, writable and executable");
			}
		}
		else {
			System.out.println("File doesnt exist");
		}
		System.setProperty("webdriver.chrome.driver","/Users/renju/Testsigma-Agent/drivers/googlechrome/78.0/chromedriver");
		driver = new ChromeDriver();
		driver.get("http://advice.milliman.com/");
	}
}
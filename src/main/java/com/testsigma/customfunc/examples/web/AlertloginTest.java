package com.testsigma.customfunc.examples.web;

import java.awt.event.KeyEvent;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Alert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class AlertloginTest {
	protected WebDriver driver;
	public static void main(String[] args) {
		try {
			AlertloginTest test = new AlertloginTest();
			test.setup();
			//test.handleAlert();
			//test.addAuth();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	}
	
	public void setup() throws MalformedURLException {
		System.setProperty("webdriver.chrome.driver", "/home/renjous/Documents/Softwares/libs/binaries/chromedriver");
		driver = new ChromeDriver();
		System.out.println("Opened chrome");
	}
	
	public void handleAlert() throws InterruptedException {
		Thread.sleep(5000);
		System.out.println("slept enough. now switch to alert and perform action");
		Alert alert = driver.switchTo().alert();
		Actions action = new Actions(driver);
		Action username = action.sendKeys("admin").keyDown(Keys.TAB).sendKeys("admin").build();
		username.perform();
	}
	
/*	public void addAuth() {
		driver.get("http://ToolsQA:TestPassword@adminrestapi.demoqa.com/authentication/CheckForAuthentication");
	}*/
}

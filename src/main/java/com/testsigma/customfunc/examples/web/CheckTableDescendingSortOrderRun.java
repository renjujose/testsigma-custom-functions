package com.testsigma.customfunc.examples.web;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.TestStepResult;

public class CheckTableDescendingSortOrderRun {
	protected WebDriver webdriver;
	protected List<WebElement> columnitems;
	protected String sortingcolumnitemspath;
	protected boolean isdescending;

	public CheckTableDescendingSortOrderRun(WebDriver webdriver){
		this.webdriver = webdriver;
		this.checkHTMLTableSortingOrder();
		webdriver.quit();
	}
	
	@CustomTestStep
	public TestStepResult checkHTMLTableSortingOrder() {
		isdescending = false;
		String sortingcolumnitemspath = "//table/tbody/tr/td/font";
		TestStepResult result= new TestStepResult();
		
		webdriver.get("https://money.rediff.com/gainers/bse/daily/groupm");
		
		//get elements of % Change column in Top gainers Table
		columnitems = webdriver.findElements(By.xpath(sortingcolumnitemspath));
		Float[] itemsfloatarray = new Float[columnitems.size()];

		//store all values to an array after removing the spaces and + symbol and converting to float
		for(int i=0;i<columnitems.size();i++) {
			itemsfloatarray[i] = Float.parseFloat(columnitems.get(i).getText().replaceAll("[/+/s]", ""));
		}
		System.out.println("Before sort after stripping +");
		for(float item:itemsfloatarray) {
			System.out.print(" "+item+",");
		}
		
		//copy the array for reversal
		Float[] itemsfloatarraydesc = itemsfloatarray.clone();
		//reverse array
		Arrays.sort(itemsfloatarraydesc, Collections.reverseOrder());
		System.out.println("After reverse sort");
		for(float item:itemsfloatarray) {
			System.out.print(" "+item+",");
		}
		if(Arrays.equals(itemsfloatarraydesc, itemsfloatarray)){
			System.out.println("The column is in descending order");
			isdescending = true;
          	result.setStatus(0);
			result.setMessage("No of items: "+columnitems.size()+"/nThe column is in descending order");
		}
		else{
			System.out.println("The column is in ascending order");	
			result.setStatus(1);
			result.setMessage("No of items: "+columnitems.size()+"/nThe column is not in descending order");
        }
      return result;
	}
	
	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "/home/renjous/Documents/Devworx/eclipse-workspace/.metadata/.plugins/org.eclipse.wst.server.core/drivers/googlechrome/2.40/chromedriver");
		CheckTableDescendingSortOrderRun test = new CheckTableDescendingSortOrderRun(new ChromeDriver());
	}
}
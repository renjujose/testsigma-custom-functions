package com.testsigma.customfunc.examples.web;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.testengine.exceptions.TestEngineException;
import org.openqa.selenium.By;

/* This Custom Function can be used to check whether the text displayed inside
a Webpage Element is same as the given verification text. The Webpage Element has
a dynamic XPath and the dynamic part is fetched from another Element in a previous step
and stored in a runtime variable.

Custom Function Parameters:
1. A runtime variable containing the dynamic value part of xpath,
2. XPath of the Element where the text is to be verified with '1replace_text1' at the 
part to be replaced,
3. Text to be verified
*/

public class VerifyCreatedDataInFrontDesk extends TestsigmaCustomFunctions {
  @CustomTestStep
  public TestStepResult verifyDisplayedText(String runtimeVariableFirstName, String runtimeVariableLastName, String runtimeVariableEmail, String runtimeVariablePhoneNumber, String UIIdentifierXpathName, String UIIdentifierXpathEmail, String UIIdentifierXpathPhoneNumber) throws TestEngineException {
    TestStepResult result = new TestStepResult();
        String StoredFirstName="";
        String StoredLastName="";
        String StoredEmail="";
        String StoredPhoneNumber="";
    try{
        StoredFirstName= getRuntimeData(runtimeVariableFirstName);
        StoredLastName= getRuntimeData(runtimeVariableLastName);
        StoredEmail= getRuntimeData(runtimeVariableEmail);
        StoredPhoneNumber= getRuntimeData(runtimeVariablePhoneNumber);
        
        //Getting the text of all the values
        String DisplayedFullName = driver.findElement(By.xpath(UIIdentifierXpathName)).getText();
        String DisplayedFirstName = DisplayedFullName.split(" ")[0];
        String DisplayedLastName = DisplayedFullName.split(" ")[1];
        String DisplayedEmail = driver.findElement(By.xpath(UIIdentifierXpathEmail)).getText();
        String DisplayedPhoneNumber = driver.findElement(By.xpath(UIIdentifierXpathPhoneNumber)).getText().split("//+91")[1];
      
      //To verify the matching of first name, last name, email and phone number
      if((StoredFirstName.equals(DisplayedFirstName)) && (StoredLastName.equals(DisplayedLastName)) && (StoredEmail.equals(DisplayedEmail)) && (StoredPhoneNumber.contains(DisplayedPhoneNumber))){
            result.setStatus(ResultConstants.SUCCESS);
            result.setMessage("All the displayed data matches with the stored data.");
          }
       else{
            result.setStatus(ResultConstants.FAILURE);
            result.setMessage("Stored Data "+StoredFirstName+" "+StoredLastName+" "+StoredEmail+" "+StoredPhoneNumber+" doesn't match with dislayed data  "+DisplayedFirstName+" "+DisplayedLastName+" "+DisplayedEmail+" "+DisplayedPhoneNumber);
      }
        }catch(TestEngineException testengex){
          result.setStatus(ResultConstants.FAILURE);
          result.setMessage("Failed to get runtime variable - TestEngineException");
        }
        return result;
    }
}

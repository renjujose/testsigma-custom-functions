package com.testsigma.customfunc.examples.web;
import org.openqa.selenium.By;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

// favicon test

public class TestForFavicon extends TestsigmaCustomFunctions {
	TestStepResult result = new TestStepResult();

	public String getGlobalParameter(String globalParameterName) {
		TestStepResult result = new TestStepResult();
		String data="";
		try {
			data = getGlobalParameterValue(globalParameterName);
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("The fetched Global Parameter " + globalParameterName + " has value: " + data);
		} catch (TestEngineException teex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to read value from Global Parameter " + teex.getMessage());
		}
		return data;
	}

	@CustomTestStep
	public TestStepResult htmlVerification(String Htmlxpath, String data, String tag) {
		String gp = getGlobalParameter(Htmlxpath);
		String str = driver.findElement(By.xpath(gp)).getAttribute(tag);

		if (str.contains(data)) {
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Test Step executed successfully. " + str);
		} else {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Test Step execution failed. " + str);
		}
		return result;
	}
}
package com.testsigma.customfunc.examples.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;


public class HTMLtablesizecheck {
	WebDriver driver = null;
	
	@Test
	public void checkTableDimensionsTest() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver","/home/renjous/Documents/Softwares/libs/binaries/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/html/html_tables.asp");
		WebElement table = driver.findElement(By.xpath("//table[@id='customers']"));
		System.out.println(table.getCssValue("height"));

	}
	
}

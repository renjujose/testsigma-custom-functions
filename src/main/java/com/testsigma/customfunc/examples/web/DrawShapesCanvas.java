package com.testsigma.customfunc.examples.web;

import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;

import org.openqa.selenium.By;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class DrawShapesCanvas extends TestsigmaCustomFunctions {
	static WebDriver wdriver;
	TestStepResult result;

	public static void main(String[] args) {
		DrawShapesCanvas dsc = new DrawShapesCanvas();
		dsc.setup();
		try {
			System.out.println(dsc.clicknDrag2DrawShape("//canvas[@class='main-canvas']", 100, 100, 200, 200).getMessage());
		} catch (IndexOutOfBoundsException indexooutboundex) {
			System.out.println(indexooutboundex.getMessage());
			//indexooutboundex.printStackTrace();
		}
		catch (InterruptedException intex) {
			System.out.println(intex.getMessage());
			//intex.printStackTrace();
		}
		finally {
			wdriver.quit();
		}
	}

	public void setup() {
		System.setProperty("webdriver.chrome.driver", "/Users/renju/Testsigma-Agent/drivers/googlechrome/78.0/chromedriver");
		wdriver = new ChromeDriver();
		wdriver.get("https://jspaint.app/");
	}
	
	
// //canvas[@class='main-canvas']	

//	@CustomTestStep
	public TestStepResult clicknDrag2DrawShape(String canvaseElementXPath, int startX, int startY, int endX, int endY) throws InterruptedException {
		result = new TestStepResult();
		Rectangle canvasrect = wdriver.findElement(By.xpath(canvaseElementXPath)).getRect();
		if(startX<canvasrect.getX()||startY<canvasrect.getY()) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Drawing Custom shape failed.");
			throw new IndexOutOfBoundsException("Shape start point less than Canvas start point. Please use a higher value for start X and start Y.");
		}
		if(endX>canvasrect.getWidth()||endY>canvasrect.getHeight()) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Drawing Custom shape failed.");
			throw new IndexOutOfBoundsException("Shape end point greater than Canvas end point. Please use a lesser value for end X and end Y.");
		}
		Actions clicknDrag = new Actions(wdriver);
		System.out.println("Wait before drag");
		Thread.sleep(2000);
		clicknDrag.moveByOffset(startX, startY).clickAndHold().moveByOffset(endX, endY).release().build().perform();
		Thread.sleep(2000);
		System.out.println("Waited afer release");
		result.setStatus(ResultConstants.SUCCESS);
		result.setMessage("Custom shape drawn successfully.");
		return result;
	}
}
package com.testsigma.customfunc.examples.web;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class CustomFunctions {
	protected WebDriver driver;
	protected String tablelocator;
	protected String sortingcolumnheadertext;
	protected List<WebElement> sortingcolumnitemslist;
	protected String sortingcolumnitemspath;
	
	@Test(priority=1)
	public void initDriver() {
		String url = "https://money.rediff.com/gainers/bsc/daily/groupa";
		System.setProperty("webdriver.chrome.driver","/home/renjous/Documents/Softwares/libs/binaries/chromedriver");
		driver = new ChromeDriver();
		driver.get(url);
		this.checkHTMLTableSortingOrder();
	}
	
	private WebElement getTable() {
		tablelocator = "//*[@id='leftcontainer']/table";
		WebElement table = driver.findElement(By.xpath(tablelocator));
		return table;
	}
	
//	@Parameters({"sortingcolumnheadertext"})
//	public WebElement getSortingColumn() {
//		sortingcolumnheadertext = "% Change";
//		String sortingcolumnlocator = "//th[contains(text(),'"+sortingcolumnheadertext+"')]";
//		System.out.println(sortingcolumnlocator);
//		WebElement sortingcolumnheader = driver.findElement(By.xpath(sortingcolumnlocator));
//		return sortingcolumnheader;
//	}
	
	private List<WebElement> getSortingColumnItems() {
		String sortingcolumnitemspath = "//table/tbody/tr/td/font";
		sortingcolumnitemslist = driver.findElements(By.xpath(sortingcolumnitemspath));
		return sortingcolumnitemslist;
	}
	
	
	public void checkHTMLTableSortingOrder() {
//		boolean isDescending = true;
		List<WebElement> sortingcolumnelementslist = getSortingColumnItems();
		float[] sortingcolumnintlist = new float[sortingcolumnelementslist.size()];
		for(int i=0;i<sortingcolumnelementslist.size();i++) {
			sortingcolumnintlist[i] = Float.parseFloat(sortingcolumnelementslist.get(i).getText().replaceAll("[^0-9.]", ""));
			System.out.println("int: "+sortingcolumnintlist[i]);
		}
		System.out.println("Count of items: "+sortingcolumnintlist.length);
		float[] sortingcolumnintlistasc = sortingcolumnintlist.clone();
		Arrays.sort(sortingcolumnintlistasc);
		if(sortingcolumnintlistasc.equals(sortingcolumnintlist))
			System.out.println("For a sorted column, the column is in ascending order");
		else {
			System.out.println("For a sorted column, the column is in descending order");
			//isDescending = false;
		}
	}
}
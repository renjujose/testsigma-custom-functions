package com.testsigma.customfunc.examples.web;

import org.openqa.selenium.WebDriver;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;


public class RuntimeTestDataManager extends TestsigmaCustomFunctions{
	protected WebDriver webdriver;
	public RuntimeTestDataManager(WebDriver webdriver){
		this.webdriver = webdriver;
	}
	
	@CustomTestStep
	public TestStepResult getRuntimeTestData(String name) throws TestEngineException {
		TestStepResult result= new TestStepResult();
		String testdata = super.getRuntimeData(name);
		result.setStatus(ResultConstants.SUCCESS);
		result.setMessage("custom step Executed successfully"+testdata);
		return result;
	}
}
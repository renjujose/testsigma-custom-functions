package com.testsigma.customfunc.examples.web;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CommandExecutor;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccessDropdown {
	WebDriver driver;
	public static void main(String[] args) throws InterruptedException {
		AccessDropdown AD = new AccessDropdown();
		AD.init();
		AD.listSelectItems();
		AD.teardown();
	}
	
	public void init(){
		System.setProperty("webdriver.gecko.driver","resources/drivers/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver,60);
		
	}
	
	public void listSelectItems(){
		driver.get("https://www.toolsqa.com/automation-practice-form");
		Select oSelect = new Select(driver.findElement(By.id("continents")));
		oSelect.selectByVisibleText("Europe");
		List<WebElement> oSize = oSelect.getOptions();
		int iListSize = oSize.size();
		for (int i = 0; i < iListSize; i++) {
			String sValue = oSelect.getOptions().get(i).getText();
			System.out.println(sValue);
			if (sValue.equals("Africa")) {
				oSelect.selectByIndex(i);
				break;

			}

		}
	}
	
	public void teardown(){
		driver.quit();
	}
	
}
package com.testsigma.customfunc.examples.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

class CricbuzzTest{
	static WebDriver driver;
	
	public static void main(String[] args){
		System.setProperty("webdriver.chrome.driver", "/home/renjous/Documents/Softwares/libs/binaries/chromedriver");
		driver = new ChromeDriver();
		driver.get("https://www.cricbuzz.com/live-cricket-scorecard/20137/sl-vs-eng-4th-odi-england-tour-of-sri-lanka-2018");
		// old selector
		//WebElement table = driver.findElement(By.cssSelector("div[class='cb-col cb-col-100 cb-ltst-wgt-hdr']"));
		// new selector
		WebElement table = driver.findElement(By.cssSelector("#innings_1 > div:first-child"));
		String textextras = table.findElement(By.cssSelector("div:nth-last-child(3) div:nth-child(2)")).getText();
		System.out.println("textextras = "+textextras);
		String textsum = table.findElement(By.cssSelector("div:nth-last-child(2) > div:nth-last-child(2)")).getText();
		System.out.println("textsum = "+textsum);
		System.out.println("Quitting webdriver");
		driver.quit();
	}
}
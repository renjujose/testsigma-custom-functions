package com.testsigma.customfunc.examples.web;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.TestStepResult;

public class CheckTableDescendingSortOrder {
	protected WebDriver webdriver;
	protected List<WebElement> columnitems;
	protected String sortingcolumnitemspath;
	protected boolean isdescending;

	public CheckTableDescendingSortOrder(WebDriver webdriver){
		this.webdriver = webdriver;
	}
	
	@CustomTestStep
	public TestStepResult checkHTMLTableSortingOrder() {
		isdescending = false;
		String sortingcolumnitemspath = "//table/tbody/tr/td/font";
		TestStepResult result= new TestStepResult();
		
		webdriver.get("https://money.rediff.com/gainers/bsc/daily/groupa");
		//get elements of % Change column in Top gainers Table
		columnitems = webdriver.findElements(By.xpath(sortingcolumnitemspath));
		Float[] itemsfloatarray = new Float[columnitems.size()];

		//store all values to an array after removing the spaces and + symbol and converting to float
		for(int i=0;i<columnitems.size();i++) {
			itemsfloatarray[i] = Float.parseFloat(columnitems.get(i).getText().replaceAll("[\\+\\s]", ""));
		}
		
		//copy the array for reversal
		Float[] itemsfloatarraydesc = itemsfloatarray.clone();
		//reverse array
		Arrays.sort(itemsfloatarraydesc, Collections.reverseOrder());

		if(itemsfloatarraydesc.equals(itemsfloatarray)) {
			System.out.println("The column is in descending order");
			isdescending = true;
		}
		else
			System.out.println("The column is in descending order");	
		result.setStatus(0);
		result.setMessage("custom step Executed successfully");
		return result;
	}
}
package com.testsigma.customfunc.examples.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class DisabledStateCheck extends TestsigmaCustomFunctions{
    TestStepResult result= new TestStepResult();
    
    @CustomTestStep
  	public TestStepResult getLinks(String links_xpath) throws TestEngineException{
    	WebElement readonlyfield = driver.findElement(By.id("email"));
    	String readonlyeleattr = readonlyfield.getAttribute("readonly");	
    	result.setStatus(ResultConstants.SUCCESS);
    	result.setMessage(readonlyeleattr);
    	return result;
  	}  
}
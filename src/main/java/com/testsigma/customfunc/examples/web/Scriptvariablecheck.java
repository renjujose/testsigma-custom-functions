package com.testsigma.customfunc.examples.web;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;


// String variableName = "platform";
// String value = "QS";
// driver.get("https://quotes.thevaninsurer.co.uk/quote/AboutYou.aspx");


public class Scriptvariablecheck extends TestsigmaCustomFunctions {
	TestStepResult result = new TestStepResult();
	WebDriver wdriver;
	
	public TestStepResult textVerification(String variableName, String value) {
		setup();
		WebElement element = wdriver.findElement(By.xpath("//script[@id='quoteDataItem']"));
		JavascriptExecutor js = (JavascriptExecutor) driver;
		String str = js.executeScript("return arguments[0].innerHTML;", element).toString();
		try {
			if (verifyValue(str, variableName, value)) {
				result.setStatus(ResultConstants.SUCCESS);
				result.setMessage("Test Step executed successfully. "+(result.getMessage()!=null?result.getMessage():""));
			} else {
				result.setStatus(ResultConstants.FAILURE);
				result.setMessage("Test Step execution failed. "+(result.getMessage()!=null?result.getMessage():""));
			}
		} catch (TestEngineException ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Test Step execution failed. "+ex.getMessage());
		}
		System.out.println(result.getMessage());
		return result;
	}

	public void setup() {
		System.setProperty("Webdriver.chrome.driver", "/Users/renju/Testsigma-Agent/drivers/googlechrome/78.0/chromedriver");
		wdriver = new ChromeDriver();
		driver.get("https://quotes.thevaninsurer.co.uk/quote/AboutYou.aspx");
	}

	
	
	public boolean verifyValue(String str, String variableName, String value) throws TestEngineException {
		String vardelimiter=",";
		int startindexofvar = str.indexOf(variableName);
		if(startindexofvar==-1) {
			TestEngineException testngex = new TestEngineException("Given variable not found in script");
			throw testngex;
		}
		int endindexofvar = str.indexOf(vardelimiter, startindexofvar);
		if(endindexofvar==-1) {
			TestEngineException testngex = new TestEngineException("Given delimiter symbol '"+
					vardelimiter+"' not found in variable declaration within the script");
			throw testngex;
		}
		String varkeyvalue = str.substring(startindexofvar, endindexofvar);
		if (varkeyvalue.contains(value)) {
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Given variable contains the specified value.");
			return true;
		} else {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Given variable doesn't contain the specified value.");
			return false;
		}
	}
}
package com.testsigma.customfunc.examples.web;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.ArrayList;
import java.util.Collection;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OrderVerification{
	boolean isArraySame = false;	
	@Test
	public void verifyDescOrder() throws InterruptedException{
//		System.setProperty("webdriver.chrome.driver","/home/renjous/Documents/Softwares/libs/binaries/chromedriver");
//		WebDriver driver = new ChromeDriver();
//		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
//		driver.manage().window().maximize();
//		driver.get("https://www.redlion.com");
//		driver.findElement(By.xpath("//input[@id='globalSearchInput']")).sendKeys("Red Lion Hotel Port Angeles Harbor");
//		driver.findElement(By.xpath("//a[text()='Red Lion Hotel Port Angeles Harbor']")).click();
//		Thread.sleep(3000);
//		driver.findElement(By.xpath("//input[@id='searchStartDate']")).click();
//		Thread.sleep(3000);
//		driver.findElement(By.xpath("//span[text()='25']")).click();
//		Thread.sleep(1000);
//		driver.findElement(By.xpath("//span[text()='26'][preceding-sibling::span]")).click();
//		Thread.sleep(1000);
//		driver.findElement(By.xpath("//button[contains(@class,'check-availability-button')]")).click();
		List<Float> pricelist = new ArrayList<Float>();
		try {
			WebElement driver = null;
			// Get list of price elements
			@SuppressWarnings("null")
			List<WebElement> price_elements = driver.findElements(By.xpath("//div[@class='price-container text-right js-temp-right-container']/p/strong[text()!='Sold Out']"));
			// Iterate,get text from elements and store it to pricelist
//			List<String> price_elements = new ArrayList<String>();
//			price_elements.add("$199.10");
//			price_elements.add("$199.20");
//			price_elements.add("C199.12");
//			price_elements.add("$199.13");
//			price_elements.add("C$199.15");
//			price_elements.add("$199.16");
//			price_elements.add("$C199.10");
			for (WebElement price_element : price_elements) {
				String price = price_element.getText();
				System.out.println(price_element);
				pricelist.add(Float.parseFloat(price.replace("$", "").replace("C", "")));
				System.out.println(pricelist);
			}
			List<Float> descpricelist = new ArrayList<Float>();
			descpricelist.addAll(pricelist);
			// sort in descending order
			Collections.sort(descpricelist, Collections.reverseOrder());
			if (descpricelist.equals(pricelist))
				isArraySame = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
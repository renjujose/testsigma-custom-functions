package com.testsigma.customfunc.examples.web;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class GetLinks extends TestsigmaCustomFunctions{
    TestStepResult result= new TestStepResult();
    @CustomTestStep
  	public TestStepResult getLinks(String links_xpath) throws TestEngineException{
    	//old article links xpath
    	//links_xpath = "//div[@class='content_column']//a[not(parent::li or @href='#')]";
    
	    List<WebElement> linksinarticle = driver.findElements(By.xpath(links_xpath));
		ArrayList<WebElement> linkslist = new ArrayList<WebElement>(linksinarticle);
      	int linkscount = linkslist.size();
		for(int i=0;i<linkscount;i++) {
			String linkurl = linkslist.get(i).getAttribute("href");
      		//setTestDataParameterValue("link"+i,linkurl);
		}
    	//setRuntimeData(name,value);
    	//String testdata = getTestDataParamterValue(name);
    	//setTestDataParameterValue(name,value);
    	result.setStatus(ResultConstants.SUCCESS);
    	result.setMessage(linkscount+" links from the article added to associated parameter test data");
    	return result;
  	}  
}
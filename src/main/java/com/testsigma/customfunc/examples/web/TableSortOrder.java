package com.testsigma.customfunc.examples.web;

import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TableSortOrder {
	protected WebDriver driver;
	protected String tablelocator;
	protected String sortingcolumnheadertext;
	protected List<WebElement> sortingcolumnitemslist;
	protected String sortingcolumnitemspath;
	
	public void initDriver() {
		//variables
		String url = "https://money.rediff.com/gainers/bsc/daily/groupa";
		
		//initialization
		System.setProperty("webdriver.chrome.driver","/home/renjous/Documents/Softwares/libs/binaries/chromedriver");
		driver = new ChromeDriver();
		
		//function code start
		driver.get(url);
		String sortingcolumnitemspath = "//table/tbody/tr/td/font";
		sortingcolumnitemslist = driver.findElements(By.xpath(sortingcolumnitemspath));
		float[] sortingcolumnintlist = new float[sortingcolumnitemslist.size()];
		for(int i=0;i<sortingcolumnitemslist.size();i++) {
			sortingcolumnintlist[i] = Float.parseFloat(sortingcolumnitemslist.get(i).getText().replaceAll("[^0-9.]", ""));
			System.out.println("int: "+sortingcolumnintlist[i]);
		}
		System.out.println("Count of items: "+sortingcolumnintlist.length);
		float[] sortingcolumnintlistasc = sortingcolumnintlist.clone();
		Arrays.sort(sortingcolumnintlistasc);
		if(sortingcolumnintlistasc.equals(sortingcolumnintlist))
			System.out.println("The column is sorted and in ascending order");
		else {
			System.out.println("For a sorted column, the column is in descending order");
			//isDescending = false;
		}
	}
}
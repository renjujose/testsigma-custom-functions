package com.testsigma.customfunc.examples.web;

/*  You need to remove this package declaration above when using
 *  this code in Custom Functions interface in Testsigma
 */

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;

public class CypressFunctionPort extends TestsigmaCustomFunctions {

	/*
	 * argument 1: selector - css selector for the element
	 */

	@CustomTestStep
	public TestStepResult clickVacButton(String selector) {
		TestStepResult result = new TestStepResult();
		try {
			WebElement vacbutton = driver.findElement(By.cssSelector(selector));
			vacbutton.findElement(By.cssSelector("button")).click();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Clicked on the button successfully");
		} catch (Exception ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to click on button!");
		}
		return result;
	}

	/*
	 * argument 1: subject - Need to know what the parameter subject is. argument 2:
	 * selector - css selector for the element argument 3: value - text to be
	 * selected in the select list
	 */

	@CustomTestStep
	public TestStepResult selectvaluefromVacDropdrown(String subject, String value, String selector) {
		TestStepResult result = new TestStepResult();
		try {
			driver.findElement(By.cssSelector(selector)).click();
			WebElement selectoverlay = driver.findElement(By.className("cdk-overlay-container"));
			selectoverlay.findElement(By.xpath(String.format("//*[text()='%s']", value))).click();
			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Selected the value from the select list successfully");
		} catch (Exception ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to select the value from the select list!");
		}
		return result;
	}

	/*
	 * argument 1: year - year to be selected argument 2: month - month to be
	 * selected argument 3: day - date to be selected argument 4: selector - css
	 * selector for the element
	 */

	@CustomTestStep
	public TestStepResult setPrecisionDate(String year, String month, String day, String selector) {
		TestStepResult result = new TestStepResult();
		try {
			driver.findElement(By.cssSelector(selector)).click();
			WebElement datepicker = driver.findElement(By.className("cdk-overlay-container"));

			WebElement yearinput = datepicker.findElement(By.className("year-select"))
					.findElement(By.cssSelector("input"));
			yearinput.clear();
			yearinput.sendKeys(year);

			WebElement monthinput = datepicker.findElement(By.className("month-select"))
					.findElement(By.cssSelector("input"));
			monthinput.clear();
			monthinput.sendKeys(month);

			WebElement dayinput = datepicker.findElement(By.className("day-select"))
					.findElement(By.cssSelector("input"));
			dayinput.clear();
			dayinput.sendKeys(day);

			WebElement datepickerbackdrop = driver.findElement(By.className("cdk-overlay-backdrop"));
			datepickerbackdrop.click();

			result.setStatus(ResultConstants.SUCCESS);
			result.setMessage("Selected the date successfully");
		} catch (Exception ex) {
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to select the date!");
		}
		return result;
	}

}
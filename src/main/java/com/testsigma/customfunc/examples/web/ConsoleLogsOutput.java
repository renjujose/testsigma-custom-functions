package com.testsigma.customfunc.examples.web;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.logging.LogEntry;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;

class ConsoleLogsOutput extends TestsigmaCustomFunctions {
	WebDriver wdriver;
	List<LogEntry> logEntriesList = null;

	@BeforeSuite
	public void setup() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver",
				"/Users/renju/Testsigma-Agent/drivers/googlechrome/78.0/chromedriver");

//		LoggingPreferences logPrefs = new LoggingPreferences();
//		logPrefs.enable(LogType.BROWSER, Level.SEVERE);
//		ChromeOptions chromeoptions = new ChromeOptions();
//		chromeoptions.setExperimentalOption("w3c", false);
//		chromeoptions.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
//		wdriver = new ChromeDriver(chromeoptions);

		wdriver = new ChromeDriver();
		wdriver.manage().timeouts().implicitlyWait(5000, TimeUnit.MILLISECONDS);
		wdriver.get("http://examples.testsigma.com/components");
		wdriver.findElement(By.cssSelector("a[href='framespage']")).click();
		Thread.sleep(3000);
	}

	@SuppressWarnings("deprecation")
	@Test
	@CustomTestStep
	public TestStepResult getLogs(String logType){
		TestStepResult result = new TestStepResult();
		//logType = LogType.BROWSER;
		//logLevel = Level.SEVERE;
		
		try {
			logEntriesList = wdriver.manage().logs().get(logType).filter(Level.parse("SEVERE"));
			int errorcount = logEntriesList.size();
			if (errorcount>0) {
				result.setStatus(ResultConstants.FAILURE);
				result.setMessage("Found "+errorcount+" errors in the browser logs. Following errors found: "+logEntriesList.toString());
			} else if(errorcount==0) {
				result.setStatus(ResultConstants.SUCCESS);
				result.setMessage("No errors found in browser logs.");
			}
			else if (logEntriesList == null) {
				result.setStatus(ResultConstants.FAILURE);
				result.setMessage("Failed to get browser logs using Custom Step.");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			result.setStatus(ResultConstants.FAILURE);
			result.setMessage("Failed to get browser logs using Custom Step.");
		}
		System.out.println(result.getMessage());
		return result;
	}
}
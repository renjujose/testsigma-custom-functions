package com.testsigma.customfunc.examples.web;

import org.openqa.selenium.WebDriver;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.result.TestStepResult;
import org.openqa.selenium.Keys;
import java.util.Random;
import java.util.stream.LongStream;
import org.openqa.selenium.By;
import java.lang.Long;

public class RandNumGenerator{
  public static WebDriver driver;
  public RandNumGenerator(WebDriver driver){
  	this.driver = driver;
  }
  @CustomTestStep
  public TestStepResult PRNGgenerate(String startvalue, String endvalue, String xpathlocator) {
	//String variablename = "randomnum1";
	Random rand = new Random();
	  //double randomnumbervalue = rand.nextInt(200000);
    int start=Integer.parseInt(startvalue);
    int end=Integer.parseInt(endvalue);
    //int end=10050;
	LongStream randomnumbervalue2 = rand.longs(1, start, end);
	  //System.out.println(randomnumbervalue);
	long[] randomnumberarray = randomnumbervalue2.toArray();
	Long randomnumber1 = randomnumberarray[0];
	String randomnumberstring = randomnumber1.toString();
	//driver.get("https://www.google.com");
	driver.findElement(By.xpath("//*[@id='lst-ib']")).sendKeys(randomnumberstring);
    //driver.findElement(By.xpath(xpathlocator)).sendKeys(randomnumberstring);
	//new RuntimeData().storeRuntimeVarible(executionID, variablename, randomnumber1, envSettings);
	TestStepResult result= new TestStepResult();
	result.setStatus(0);
	result.setMessage("custom step Executed successfully");
	return result;
  }
}

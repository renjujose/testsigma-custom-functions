package com.testsigma.customfunc.examples.web;

import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;
import org.openqa.selenium.JavascriptExecutor;

public class VideoPlayerTimeCheck extends TestsigmaCustomFunctions{
	TestStepResult result= new TestStepResult();
  
    @CustomTestStep
	public TestStepResult getCurrentPlaybackTime(String videoplayerID) throws TestEngineException{
		JavascriptExecutor jsexec=(JavascriptExecutor)driver;
		String videotime = jsexec.executeScript("var myPlayer = videojs.getPlayer(arguments[0]);return myPlayer.currentTime();", videoplayerID).toString();
    	result.setStatus(ResultConstants.SUCCESS);
    	result.setMessage("Current time value is "+videotime);
    	return result;
  	}
}
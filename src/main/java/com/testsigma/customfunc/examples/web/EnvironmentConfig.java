package com.testsigma.customfunc.examples.web;

import com.testsigma.customfunc.common.CustomTestData;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.Capabilities;

public class EnvironmentConfig extends TestsigmaCustomFunctions {

	@CustomTestData
	public String getCurrentBrowserName(){
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = cap.getBrowserName().toLowerCase();
		return browserName;
	}
	
	@CustomTestData
	public String getCurrentOS(){
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String OSName = cap.getPlatform().toString();
		return OSName;
	}
	
	
	@CustomTestData
	public String getCurrentBrowserVersion() throws Exception {
		Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
		String browserName = cap.getVersion().toLowerCase();
		return browserName;
	}
}
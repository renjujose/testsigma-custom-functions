package com.testsigma.customfunc.examples;

import com.testsigma.customfunc.common.CustomTestData;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class RuntimeParameterTestDataExample extends TestsigmaCustomFunctions {

	@CustomTestData
	public String getRuntimeParameterTestData(String name) throws TestEngineException {
		String testData = getRuntimeData(name);
		return testData;

	}
	
	@CustomTestData
	public String storeRuntimeParameterTestData(String name, String value) throws TestEngineException {
		setRuntimeData(name, value);
		return value;

	}
	
	@CustomTestStep
	public TestStepResult getRuntimeParameter(String name) throws TestEngineException {
		TestStepResult result = new TestStepResult();
		String testData = getRuntimeData(name);
		// use the value stored in testData variable now
		result.setStatus(ResultConstants.SUCCESS);
		result.setMessage("Custom step Executed successfully. Value of the runtime parameter "+name+" is "+testData);
		return result;
	}
	
	@CustomTestStep
	public TestStepResult storeRuntimeParameter(String name, String value) throws TestEngineException {
		TestStepResult result = new TestStepResult();
		// get the value to be stored into runtime variable
		setRuntimeData(name, value);
		result.setStatus(ResultConstants.SUCCESS);
		result.setMessage("custom step Executed successfully");
		result.getMetadata().put("metadatakey", "metadataresult");
		return result;
	}

}

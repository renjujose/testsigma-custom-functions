package com.testsigma.customfunc.examples.android;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;

public class TestObjectWithAppCloudUrl {
	public static void main(String args[]) throws Exception {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		//We can pass the App ID or the local App Path here. For more info, please check TestObject documentation.
		capabilities.setCapability("testobjectApiKey", "your_testobject_api_key");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("deviceName", "LG_Nexus_5X_real");
		capabilities.setCapability("testobject_app_id", "43");
        capabilities.setCapability("name", "My Test 3!");
        AppiumDriver driver = new AppiumDriver(new URL("https://us1.appium.testobject.com/wd/hub"), capabilities);
        driver.launchApp();
	}

}

package com.testsigma.customfunc.examples.android;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class BrowserStackWithAppCloudUrl {

	//Enter your BrowserStack User and Accesskey here
	public static String userName = "testuser";
	public static String accessKey = "your_access_key";
	
	public static void main(String args[]) throws Exception {
		
	DesiredCapabilities caps = new DesiredCapabilities();
	caps.setCapability("device", "Google Nexus 6");
	//The direct URL for the app uploaded to BrowserStack
	//We can pass the App ID or the local App Path here. For more info, please check Browserstack documentation
    caps.setCapability("app", "bs://e139ff6b5d13d43ed22f0a661c670e5ff718249e");
	
	AndroidDriver driver = new AndroidDriver(new URL("https://"+userName+":"+accessKey+"@hub-cloud.browserstack.com/wd/hub"),caps);
    driver.launchApp();
	System.out.println("working");
	}

}

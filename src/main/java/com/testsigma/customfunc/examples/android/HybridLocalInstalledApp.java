package com.testsigma.customfunc.examples.android;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;

public class HybridLocalInstalledApp {
	private AndroidDriver driver;
	public static void main(String args[]) throws Exception{
		HybridLocalInstalledApp demo = new HybridLocalInstalledApp();
		demo.setup();
	}
	private void setup() throws MalformedURLException {
		DesiredCapabilities cap = new DesiredCapabilities(); 
		cap.setCapability("platformName", "Android");
		cap.setCapability("platformVersion", "9");
		cap.setCapability("deviceName", "99c26a69");
		cap.setCapability("appPackage", "org.wikipedia");
		cap.setCapability("appActivity", ".main.MainActivity");
		driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		//AppiumDriver<?> driver = new AppiumDriver(new URL("http://0.0.0.0:4723/wd/hub"),cap);
		WebElement continuebutton = driver.findElement(By.xpath("//android.widget.ImageView[@content-desc='Continue']"));
		continuebutton.click();
		continuebutton.click();
		continuebutton.click();
		driver.findElement(By.xpath("//android.widget.Switch")).click();
		Activity msgApp = new Activity("com.moez.QKSMS", "com.moez.QKSMS.feature.main.MainActivity");
		Activity wikiApp = new Activity("org.wikipedia",".main.MainActivity");
		driver.startActivity(msgApp);
		System.out.println("orking fine");
		driver.quit();
	}	
}

/*How to find appPackage and appActivity
Connect device to your system
In command prompt type 
1.adb shell
2.dumpsys window windows | grep -E ‘mCurrentFocus|mFocusedApp’*/
//Example
//For WhatsApp aapPackage is com.whatsapp and the aapActivity is com.whatsapp.HomeActivity.





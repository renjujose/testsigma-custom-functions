package com.testsigma.customfunc.examples.android;

import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;

public class HybridCloudAppUrlTest {
	public static void main(String args[]) throws Exception{
		DesiredCapabilities cap = new DesiredCapabilities(); 
		cap.setCapability("platformName", "Android");
		cap.setCapability("deviceName", "6cebaa96");
		//The Direct link to your App that is uploaded to Cloud such as Amazon S3, Dropbox e.t.c. You can also pass your local file URL here.
		cap.setCapability("app", "https://www.sample.com/path/to/your/app.apk");
		AndroidDriver driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),cap);
		driver.launchApp();

		System.out.println("working fine");
		driver.quit();
	}
}

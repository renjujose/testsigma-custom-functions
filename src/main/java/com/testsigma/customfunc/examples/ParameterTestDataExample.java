package com.testsigma.customfunc.examples;


import com.testsigma.customfunc.common.CustomTestData;
import com.testsigma.customfunc.common.CustomTestStep;
import com.testsigma.customfunc.common.TestsigmaCustomFunctions;
import com.testsigma.customfunc.result.ResultConstants;
import com.testsigma.customfunc.result.TestStepResult;
import com.testsigma.testengine.exceptions.TestEngineException;

public class ParameterTestDataExample extends TestsigmaCustomFunctions{

	@CustomTestData
	public String getParameterTestData(String name) throws TestEngineException {
		String testData = getTestDataParameterValue(name);
		return testData;

	}
	
	@CustomTestData
	public String storeParameterTestData(String name, String value) throws TestEngineException {
		setTestDataParameterValue(name,value);
		return value;

	}
	
	
    @CustomTestStep
  public TestStepResult storeTestDataParameter(String name, String value) throws TestEngineException{
    
    setTestDataParameterValue(name,value);
    TestStepResult result= new TestStepResult();
    result.setStatus(ResultConstants.SUCCESS);
    result.setMessage("Parameter Testdata variable "+name+" with value "+value+" has been stored successfully");
    return result;
  }
  
    @CustomTestStep
  public TestStepResult getTestDataParameter(String name) throws TestEngineException{

    String testData = getTestDataParameterValue(name);
    TestStepResult result= new TestStepResult();
    result.setStatus(ResultConstants.SUCCESS);
    result.setMessage("The fetched Parameter Testdata variable "+name+" has value: "+testData);
    return result;
  }
}
